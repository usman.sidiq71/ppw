from django import forms

class Activity_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }
    attrs = {
        'class': 'form-control',
        'placeholder': 'insert input here',
    }
    
    day = forms.CharField(label='Hari', required=True, max_length=27, widget=forms.TextInput(attrs=attrs))
    date = forms.DateField(label='Tanggal', required=True, widget=forms.DateInput(attrs=attrs))
    hour = forms.TimeField(label='Jam', required=True, widget=forms.TimeInput(attrs=attrs))
    name = forms.CharField(label='Nama Kegiatan', required=True, widget=forms.TextInput(attrs=attrs))
    place = forms.CharField(label='Tempat', max_length=100, widget=forms.TextInput(attrs=attrs), required=True)
    category = forms.CharField(label='Kategori', max_length=100, widget=forms.TextInput(attrs=attrs), required=True)
from django.urls import path, re_path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('register/', views.register, name='register'),
    path('activity/', views.add_Activity, name='activity'),
    re_path(r'^list/', views.show_Activity, name='list'),
    path('reschedule/', views.del_Activity, name='delete'),
]
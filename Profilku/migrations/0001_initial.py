# Generated by Django 2.1.1 on 2018-10-04 07:51

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Activity',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('day', models.CharField(max_length=27)),
                ('date', models.DateField(max_length=27)),
                ('hour', models.TimeField(max_length=27)),
                ('name', models.CharField(max_length=100)),
                ('place', models.CharField(max_length=100)),
                ('category', models.CharField(max_length=100)),
            ],
        ),
    ]

from django.db import models
    
class Activity(models.Model):
	day = models.CharField(max_length=27)
	date = models.DateField(max_length=27)
	hour = models.TimeField(max_length=27)
	name = models.CharField(max_length=100)
	place = models.CharField(max_length=100)
	category = models.CharField(max_length=100)

def __str__(self):
    return self.message

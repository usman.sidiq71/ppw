from django.shortcuts import render, redirect
from .forms import Activity_Form
from .models import Activity

def index(request):
    return render(request, 'Profilku/index.html')

def register(request):
	return render(request, 'Profilku/register.html')
# Create your views here.
def reg_Activity(request):
	return render(request, 'Profilku/activity.html', {'reg_act: Activity_Form'})

def show_Activity(request):
	return render(request, 'Profilku/list.html', {'jadwal': Activity.objects.all()})

def del_Activity(request):
	return render(request, 'Profilku/list.html', {'jadwal': Activity.objects.all().delete()})
	
def add_Activity(request):
    form = Activity_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        day = request.POST['day']
        date = request.POST['date']
        hour = request.POST['hour']
        name = request.POST['name']
        place = request.POST['place']
        category = request.POST['category']
        activity_List = Activity(day=day,
        						date=date,
        						hour=hour,
        						name=name,
        						place=place,
        						category=category)
        activity_List.save()
        return redirect('/list')

    else:
        return render(request, 'Profilku/activity.html', {'jadwal': Activity.objects.all(), 'reg_act':form})
